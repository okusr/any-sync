syntax = "proto3";
package coordinator;

option go_package = "coordinator/coordinatorproto";

service Coordinator {
    // SpaceSign signs a space creation operation
    rpc SpaceSign(SpaceSignRequest) returns (SpaceSignResponse);

    // FileLimitCheck checks a limit by account and space
    // can be used only:
    //  - if a handshake identity matches a given identity
    //  - if a requester contains in nodeconf list
    rpc FileLimitCheck(FileLimitCheckRequest) returns (FileLimitCheckResponse);

    // SpaceStatusCheck checks the status of space and tells if it is deleted or not
    rpc SpaceStatusCheck(SpaceStatusCheckRequest) returns (SpaceStatusCheckResponse);

    // SpaceStatusChange changes the status of space
    rpc SpaceStatusChange(SpaceStatusChangeRequest) returns (SpaceStatusChangeResponse);

    // NetworkConfiguration retrieves the latest network configuration
    rpc NetworkConfiguration(NetworkConfigurationRequest) returns (NetworkConfigurationResponse);
}

message SpaceSignRequest {
    // SpaceId is the id of the signed space
    string spaceId = 1;
    // Header is the header of the signed space
    bytes header = 2;
    // OldIdentity is the old identity of the space owner
    bytes oldIdentity = 3;
    // NewIdentitySignature is the new identity signed by the old one
    bytes newIdentitySignature = 4;
}

enum ErrorCodes {
    Unexpected = 0;
    SpaceDeleted = 1;
    SpaceDeletionPending = 2;
    SpaceCreated = 3;
    SpaceNotExists = 4;
    ErrorOffset = 300;
}

enum SpaceStatus {
    SpaceStatusCreated = 0;
    SpaceStatusPendingDeletion = 1;
    SpaceStatusDeletionStarted = 2;
    SpaceStatusDeleted = 3;
}

message SpaceStatusPayload {
    SpaceStatus status = 1;
    int64 deletionTimestamp = 2;
}

message SpaceSignResponse {
    SpaceReceiptWithSignature receipt = 1;
}

// SpaceReceiptWithSignature contains protobuf encoded receipt and its signature
message SpaceReceiptWithSignature {
    bytes spaceReceiptPayload = 1;
    bytes signature = 2;
}

// SpaceReceipt contains permission to SpacePush operation
message SpaceReceipt {
    // SpaceId is the identifier of space
    string spaceId = 1;
    // PeerId of receipt requester
    string peerId = 2;
    // AccountIdentity is an identity of a space owner
    bytes accountIdentity = 3;
    // NetworkId is the id of a network where the receipt is issued
    string networkId = 4;
    // ValidUntil is a unix-timestamp with a deadline time of receipt validity
    uint64 validUntil = 5;
}

// FileLimitCheckRequest contains an account identity and spaceId
// control node checks that identity owns a given space
message FileLimitCheckRequest {
    bytes accountIdentity = 1;
    string spaceId = 2;
}

// FileLimitCheckResponse returns a current space limit in bytes
message FileLimitCheckResponse {
    uint64 limit = 1;
}

// SpaceStatusCheckRequest contains the spaceId of requested space
message SpaceStatusCheckRequest {
    string spaceId = 1;
}

// SpaceStatusCheckResponse contains the current status of space
message SpaceStatusCheckResponse {
    SpaceStatusPayload payload = 1;
}

// SpaceStatusChangeRequest contains the deletionChange if we want to delete space, or it is empty otherwise
message SpaceStatusChangeRequest {
    string spaceId = 1;
    string deletionChangeId = 2;
    bytes deletionChangePayload = 3;
}

// SpaceStatusChangeResponse contains changed status of space
message SpaceStatusChangeResponse {
    SpaceStatusPayload payload = 1;
}

// NetworkConfigurationRequest contains currenId of the client configuration, it can be empty
message NetworkConfigurationRequest {
    // currenId of the client configuration
    // if the currentId is equal to the latest configuration id then the response will not contain a nodes list
    string currentId = 1;
}

// NetworkConfigurationResponse contains list of nodes
message NetworkConfigurationResponse {
    // id of current configuration
    string configurationId = 1;
    // network id
    string networkId = 2;
    // nodes list - it will be empty if the client's currentId is equal configurationId
    repeated Node nodes = 3;
    // unix timestamp of the creation time of configuration
    uint64 creationTimeUnix = 4;
}

// NodeType determines the type of API that a node supports
enum NodeType {
    // TreeAPI supports space/tree sync api
    TreeAPI = 0;
    // FileAPI support file api
    FileAPI = 1;
    // CoordinatorAPI supports coordinator api
    CoordinatorAPI = 2;
}

// Node describes one node in the network
message Node {
    // peerId - it's a peer identifier (libp2p format string) so it's an encoded publicKey
    string peerId = 1;
    // list of node addresses
    repeated string addresses = 2;
    // list of supported APIs
    repeated NodeType types = 3;
}
